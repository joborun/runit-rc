#!/bin/bash

# sourcing our current rc.conf requires this to be a bash script
. /usr/lib/rc/functions

case "$1" in
	start)
		stat_busy "Running binfmt.d"
		/usr/lib/jobo/binfmt.sh || stat_die binfmt.d
		add_daemon binfmt.d
		stat_done binfmt.d
		;;
	*)
		echo "usage: $0 {start|once}"
		exit 1
		;;
esac
